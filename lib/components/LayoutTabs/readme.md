LayoutTabs are intended to provide a way to navigate between distinct sections within the layout without leaving the page or triggering a page refresh.

```js
<Layout padding background>
  <LayoutTabs.Container>
    <LayoutTabs.Tab active>Details</LayoutTabs.Tab>
    <LayoutTabs.Tab>Additional information</LayoutTabs.Tab>
    <LayoutTabs.Tab notification="2">Map</LayoutTabs.Tab>
  </LayoutTabs.Container>
</Layout>
```
